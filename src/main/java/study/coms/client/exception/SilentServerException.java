package study.coms.client.exception;

public final class SilentServerException extends ServerException {

    public SilentServerException(int code, String message) {
        super(false, code, message);
    }
}
