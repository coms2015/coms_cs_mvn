package study.coms.client.exception;

public class ServerException extends RuntimeException {
    private boolean critical;
    private int code;

    public boolean isCritical() {
        return critical;
    }

    public int getCode() {
        return code;
    }

    public ServerException(boolean critical, int code, String message) {
        super(message);
        this.critical = critical;
        this.code = code;
    }
}
