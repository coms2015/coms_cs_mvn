package study.coms.client;

import study.coms.client.helper.ExceptionHandler;
import study.coms.client.view.Store;
import study.coms.client.view.User;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ComsForm {
    private JTabbedPane tabsPane;
    private JPanel mainPanel;
    private JPanel storeTab;
    private JTable storeTable;
    private JButton storeRefreshButton;
    private JButton storeAddButton;
    private JPanel goodsTab;
    private JTable goodsTable;
    private JButton goodsAddButton;
    private JButton goodsRefreshButton;

    private static final User user = new User();
    private final Store store;

    public ComsForm() {
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler::handleException);

        storeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        store = new Store(storeTable);
        storeRefreshButton.addActionListener(e -> store.refreshTable());
        storeAddButton.addActionListener(e -> store.add());
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(
                UIManager.getSystemLookAndFeelClassName());

        JFrame frame = new JFrame("Coms");
        ComsForm form = new ComsForm();
        frame.setContentPane(form.mainPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                user.logout();
            }
        });
        if (!user.showLoginForm())
            return;
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
