package study.coms.client.view;

import study.coms.client.service.ServicesProvider;
import study.coms.client.service.StoreService;
import study.coms.client.view.dialog.StoreDialog;
import study.coms.client.view.dialog.WaitDialog;
import study.coms.client.view.model.StoreTableModel;
import study.coms.exchange.store.XStoreDto;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class Store extends AbstractView<XStoreDto> {
    private final StoreService storeService = ServicesProvider.getInstance().getStoreService();
    private final JTable table;
    private final StorePopUp popUp = new StorePopUp();
    private final StoreTableModel tableModel = new StoreTableModel();

    public Store(JTable table) {
        this.table = table;
        table.setModel(tableModel);
        table.setAutoCreateRowSorter(true);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    table.setRowSelectionInterval(table.rowAtPoint(e.getPoint()), table.rowAtPoint(e.getPoint()));
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        popUp.setSelectedStore(tableModel.getStoreAt(index)).show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
    }

    @Override
    protected void onDialogOK(XStoreDto data) {
        if (data.getN() == null)
            storeService.addStoreAsync(data, f -> onOperationResult());
        else
            storeService.editStoreAsync(data, f -> onOperationResult());
        WaitDialog.createAndShowWaitDialog();
    }

    @Override
    public void add() {
        new StoreDialog(this::onDialogOK, null).showDialog();
    }

    @Override
    public void onGetResult(List<XStoreDto> stores) {
        WaitDialog.closeDialog();
        tableModel.setStores(stores);
        tableModel.fireTableDataChanged();
    }

    @Override
    public void onOperationResult() {
        WaitDialog.closeDialog();
        refreshTable();
    }

    private void retrieveStores() {
        storeService.getStoresListAsync(this::onGetResult);
        WaitDialog.createAndShowWaitDialog();
    }

    @Override
    public void refreshTable() {
        retrieveStores();
        table.getColumn("Склад").setPreferredWidth(3000);
        table.getColumn("Адрес").setPreferredWidth(7000);
    }

    private class StorePopUp extends JPopupMenu {
        private XStoreDto selectedStore;

        public StorePopUp() {
            super();
            JMenuItem item = new JMenuItem("Изменить...");
            item.addActionListener(e -> onEditClick());
            add(item);
            item = new JMenuItem("Удалить");
            item.addActionListener(e -> onRemoveClick());
            add(item);
        }

        public StorePopUp setSelectedStore(XStoreDto selectedStore) {
            this.selectedStore = selectedStore;
            return this;
        }

        private void onEditClick() {
            new StoreDialog(Store.this::onDialogOK, selectedStore).showDialog();
        }

        private void onRemoveClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены?\nУдаление склада приведет к удалению всего СТОКА и ДОКУМЕНТОВ данного склада. Эта операция не обратима", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                storeService.removeStoreAsync(selectedStore, f -> Store.this.onOperationResult());
            }
        }
    }
}
