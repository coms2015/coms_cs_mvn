package study.coms.client.view;

import org.jdesktop.swingx.JXLoginPane;
import study.coms.client.service.AuthService;
import study.coms.client.service.ServicesProvider;

import javax.swing.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {
    private final AuthService authService = ServicesProvider.getInstance().getAuthService();

    public boolean showLoginForm() {
        JXLoginPane.JXLoginDialog dialog = new JXLoginPane.JXLoginDialog((JFrame) null, new JXLoginPane(new LoginService()));
        dialog.setAutoRequestFocus(true);
        dialog.setVisible(true);
        return dialog.getStatus() == JXLoginPane.Status.SUCCEEDED;
    }

    public void logout() {
        authService.logout();
    }

    private final class LoginService extends org.jdesktop.swingx.auth.LoginService {
        @Override
        public boolean authenticate(String login, char[] passChars, String s1) throws Exception {
            String password = new String(passChars);
            return authService.login(login, SHA1Hasher.SHA1(password));
        }
    }
}


class SHA1Hasher {
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash).toLowerCase();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}