package study.coms.client.view.model;

import study.coms.exchange.store.XStoreDto;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class StoreTableModel extends AbstractTableModel {
    private List<XStoreDto> stores = new ArrayList<>(0);

    public void setStores(List<XStoreDto> stores) {
        this.stores.clear();
        this.stores = stores;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Склад";
            case 1:
                return "Адрес";
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        return stores.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return stores.get(rowIndex).getName();
            case 1:
                return stores.get(rowIndex).getAddress();
        }
        return null;
    }

    public XStoreDto getStoreAt(int rowIndex) {
        return stores.get(rowIndex);
    }
}
