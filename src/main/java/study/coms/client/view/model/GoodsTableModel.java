package study.coms.client.view.model;

import study.coms.exchange.goods.XGoodsDto;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class GoodsTableModel extends AbstractTableModel {
    private List<XGoodsDto> goods = new ArrayList<>(0);

    public void setGoods(List<XGoodsDto> goods) {
        this.goods.clear();
        this.goods = goods;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Код";
            case 1:
                return "Наименование";
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getRowCount() {
        return goods.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return goods.get(rowIndex).getCode();
            case 1:
                return goods.get(rowIndex).getName();
        }
        return null;
    }

    public XGoodsDto getGoodsAt(int rowIndex) {
        return goods.get(rowIndex);
    }
}
