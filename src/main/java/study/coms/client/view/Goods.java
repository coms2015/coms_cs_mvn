package study.coms.client.view;

import study.coms.client.service.GoodsService;
import study.coms.client.service.ServicesProvider;
import study.coms.client.view.AbstractView;
import study.coms.client.view.dialog.WaitDialog;
import study.coms.client.view.model.GoodsTableModel;
import study.coms.exchange.goods.XGoodsDto;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class Goods extends AbstractView<XGoodsDto> {
    private final GoodsService goodsService = ServicesProvider.getInstance().getGoodsService();
    private final JTable table;
    private final GoodsPopUp popUp = new GoodsPopUp();
    private final GoodsTableModel tableModel = new GoodsTableModel();

    public Goods(JTable table) {
        this.table = table;
        table.setModel(tableModel);
        table.setAutoCreateRowSorter(true);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    table.setRowSelectionInterval(table.rowAtPoint(e.getPoint()), table.rowAtPoint(e.getPoint()));
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        popUp.setSelectedGoods(tableModel.getGoodsAt(index)).show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
    }

    @Override
    protected void onDialogOK(XGoodsDto data) {
        if (data.getN() == null)
            goodsService.addGoodsAsync(data, f -> onOperationResult());
        else
            goodsService.editGoodsAsync(data, f -> onOperationResult());
    }

    @Override
    public void onGetResult(List<XGoodsDto> data) {
        WaitDialog.closeDialog();
        tableModel.setGoods(data);
        tableModel.fireTableDataChanged();
    }

    @Override
    public void onOperationResult() {
        WaitDialog.closeDialog();
        refreshTable();
    }

    @Override
    public void refreshTable() {
        goodsService.getGoodsListAsync(this::onGetResult);
        WaitDialog.createAndShowWaitDialog();
        table.getColumn("Код").setPreferredWidth(3000);
        table.getColumn("Наименование").setPreferredWidth(7000);
    }

    @Override
    public void add() {

    }

    private class GoodsPopUp extends JPopupMenu {
        private XGoodsDto selectedGoods;

        public GoodsPopUp() {
            super();
            JMenuItem item = new JMenuItem("Изменить...");
            item.addActionListener(e -> onEditClick());
            add(item);
            item = new JMenuItem("Удалить");
            item.addActionListener(e -> onRemoveClick());
            add(item);
        }

        public GoodsPopUp setSelectedGoods(XGoodsDto selectedGoods) {
            this.selectedGoods = selectedGoods;
            return this;
        }

        private void onEditClick() {

        }

        private void onRemoveClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены?\nУдаление товара приведет к удалению всех его из всех документов и стока. Эта операция не обратима", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                goodsService.removeGoodsAsync(selectedGoods, f -> onOperationResult());
            }
        }
    }
}
