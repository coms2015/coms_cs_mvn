package study.coms.client.view.dialog;

import study.coms.exchange.goods.XGoodsDto;

import javax.swing.*;
import java.util.function.Consumer;

public class GoodsDialog extends RootEditDialog<XGoodsDto> {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    public GoodsDialog(Consumer<XGoodsDto> resultFunction, XGoodsDto data) {
        /* start root init */
        super(resultFunction, data == null ? new XGoodsDto() : data);
        this.setLinkage(contentPane, buttonOK, buttonCancel);
        /* end root init */
        this.setTitle(data == null ? "Новый товар" : "Редактирование товара");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
    }

    @Override
    protected boolean isModified() {
        return false;
    }

    @Override
    protected void readData() {

    }

    @Override
    protected void setData() {

    }
}
