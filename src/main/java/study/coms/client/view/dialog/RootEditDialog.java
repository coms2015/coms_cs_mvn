package study.coms.client.view.dialog;

import com.sun.istack.internal.NotNull;
import study.coms.exchange.IValueObject;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

public abstract class RootEditDialog<IVO extends IValueObject> extends JDialog {
    protected final IVO data;
    private final Consumer<IVO> resultFunction;

    public RootEditDialog(Consumer<IVO> resultFunction, @NotNull IVO data) {
        super();
        this.resultFunction = resultFunction;
        this.data = data;
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    public final void setLinkage(@NotNull JPanel contentPane, @NotNull JButton buttonOK, @NotNull JButton buttonCancel) {
        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        setData();
    }

    public final void showDialog() {
        this.pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void onOK() {
        if (resultFunction != null && isModified()) {
            readData();
            resultFunction.accept(data);
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    protected abstract boolean isModified();

    protected abstract void readData();

    protected abstract void setData();
}
