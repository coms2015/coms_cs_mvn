package study.coms.client.view.dialog;

import javax.swing.*;
import java.awt.*;

public class WaitDialog {
    private static volatile JDialog waitDialog = null;

    private WaitDialog() {}

    public static void createAndShowWaitDialog() {
        if (SwingUtilities.isEventDispatchThread()) {
            if (waitDialog != null) {
                waitDialog.dispose();
            }
            waitDialog = new JDialog((JFrame) null, "coms", true);
            JProgressBar progressBar = new JProgressBar(0, 100);
            progressBar.setIndeterminate(true);
            waitDialog.add(BorderLayout.CENTER, progressBar);
            waitDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            waitDialog.setSize(300, 20);
            waitDialog.setLocationRelativeTo(null);
            waitDialog.setUndecorated(true);
            waitDialog.setVisible(true);
        }
    }

    public static void closeDialog() {
        if (waitDialog != null) {
            waitDialog.setVisible(false);
            waitDialog.dispose();
            waitDialog = null;
        }
    }
}
