package study.coms.client.view.dialog;

import study.coms.exchange.store.XStoreDto;

import javax.swing.*;
import java.util.function.Consumer;

public class StoreDialog extends RootEditDialog<XStoreDto> {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameField;
    private JTextArea addressArea;

    public StoreDialog(Consumer<XStoreDto> resultFunction, XStoreDto data) {
        /* start root init */
        super(resultFunction, data == null ? new XStoreDto() : data);
        this.setLinkage(contentPane, buttonOK, buttonCancel);
        /* end root init */
        this.setTitle(data == null ? "Новый склад" : "Редактирование склада");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
    }

    @Override
    protected void readData() {
        data.setName(nameField.getText());
        data.setAddress(addressArea.getText());
    }

    @Override
    protected boolean isModified() {
        return (nameField.getText() != null ? !nameField.getText().equals(data.getName()) : data.getName() != null) || (addressArea.getText() != null ? !addressArea.getText().equals(data.getAddress()) : data.getAddress() != null);
    }

    @Override
    protected void setData() {
        nameField.setText(data.getName());
        addressArea.setText(data.getAddress());
    }
}
