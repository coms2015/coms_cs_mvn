package study.coms.client.view;

import study.coms.exchange.IValueObject;

import java.util.List;

public abstract class AbstractView<X extends IValueObject> {
    protected abstract void onDialogOK(X data);
    public abstract void onGetResult(List<X> data);
    public abstract void onOperationResult();
    public abstract void refreshTable();
    public abstract void add();
}
