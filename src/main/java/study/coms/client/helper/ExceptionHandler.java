package study.coms.client.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import study.coms.client.exception.ServerException;
import study.coms.client.exception.SilentServerException;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutionException;

public final class ExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    public static void handleException(Thread thread, Throwable e) {
        if (e instanceof ExecutionException)
            e = e.getCause();
        if (e instanceof ServerException) {
            if (((ServerException) e).isCritical()) {
                logger.error(String.format("CriticalServerError in thread %d", thread.getId()), e.getMessage());
                JOptionPane.showMessageDialog(null, "Критическая ошибка: " + e.getMessage() + "\n(" + e.toString() + ")", "COMS", JOptionPane.ERROR_MESSAGE);
                for (Frame frame : Frame.getFrames())
                    frame.dispose();
            } else if (e instanceof SilentServerException) {
                logger.info(String.format("Muted exception in thread %d", thread.getId()), e);
            } else {
                logger.warn(String.format("ServerError in thread %s", thread.getId()), e);
                JOptionPane.showMessageDialog(null, e.getMessage() + "\n(" + e.toString() + ")", "COMS", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            logger.error(String.format("Uncaught exception in thread %d", thread.getId()), e);
            JOptionPane.showMessageDialog(null, "Критическая ошибка: " + e.getMessage() + "\n(" + e.toString() + ")", "COMS", JOptionPane.ERROR_MESSAGE);
            //TODO только на время разработки
            if (JOptionPane.showConfirmDialog(null, e.getMessage() + "\nПродолжить выполнение программы?", "COMS", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) == JOptionPane.NO_OPTION) {
                for (Frame frame : Frame.getFrames()) {
                    frame.dispose();
                }
            }
        }
    }
}
