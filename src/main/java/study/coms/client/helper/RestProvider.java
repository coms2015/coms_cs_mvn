package study.coms.client.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import study.coms.client.exception.ServerException;
import study.coms.client.service.AuthService;
import study.coms.client.service.ServicesProvider;
import study.coms.exchange.message.XErrorResp;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/* class is abstract to enable obtain generic-types at runtime */
public abstract class RestProvider<RequestDto, ResponseDto> {
    private final AuthService authService = ServicesProvider.getInstance().getAuthService();
    private Consumer<ServerException> exceptionConsumer = null;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private ExecutorService executor = null;

    public RestProvider() {}

    public RestProvider(Consumer<ServerException> exceptionConsumer) {
        this.exceptionConsumer = exceptionConsumer;
    }

    public ResponseDto get(String path) {
        return _get(path);
    }

    public List<ResponseDto> getList(String path) {
        return _getList(path);
    }

    public ResponseDto post(String path, RequestDto requestDto) {
        return _post(path, requestDto);
    }

    public void getAsync(String path, Consumer<ResponseDto> onResult) {
        if (executor == null)
            executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            onResult.accept(_get(path));
            return null;
        });
    }

    public void getListAsync(String path, Consumer<List<ResponseDto>> onResult) {
        if (executor == null)
            executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            onResult.accept(_getList(path));
            return null;
        });
    }

    public void postAsync(String path, RequestDto requestDto, Consumer<ResponseDto> onResult) {
        if (executor == null)
            executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            onResult.accept(_post(path, requestDto));
            return null;
        });
    }

    private ResponseDto _get(String path) {
        WebResource resource = Client.create().resource(PropertyHelper.getProperty("server_url") + path);
        ClientResponse response = resource.accept(MediaType.APPLICATION_JSON_TYPE).cookie(new NewCookie("JSESSIONID", authService.getSid())).get(ClientResponse.class);
        authCookies(response.getCookies());
        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            try {
                return objectMapper.readValue(response.getEntity(String.class), getResponseClass());
            } catch (IOException e) {
                throw new RuntimeException("Server connection error");
            }
        } else {
            ServerException serverException;
            try {
                XErrorResp errorResp = objectMapper.readValue(response.getEntity(String.class), XErrorResp.class);
                serverException = new ServerException(false, errorResp.getErrCode(), errorResp.getErrMessage());
            } catch (IOException e) {
                serverException = new ServerException(true, response.getStatus(), response.getEntity(String.class));
            }
            if (exceptionConsumer != null) {
                exceptionConsumer.accept(serverException);
                return null;
            } else
                throw serverException;
        }
    }

    private List<ResponseDto> _getList(String path) {
        WebResource resource = Client.create().resource(PropertyHelper.getProperty("server_url") + path);
        ClientResponse response = resource.accept(MediaType.APPLICATION_JSON_TYPE).cookie(new NewCookie("JSESSIONID", authService.getSid())).get(ClientResponse.class);
        authCookies(response.getCookies());
        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            if (response.hasEntity()) {
                try {
                    return objectMapper.readValue(response.getEntity(String.class), objectMapper.getTypeFactory().constructCollectionType(List.class, getResponseClass()));
                } catch (IOException e) {
                    throw new RuntimeException("Server connection error");
                }
            } else
                return null;
        } else {
            ServerException serverException;
            try {
                XErrorResp errorResp = objectMapper.readValue(response.getEntity(String.class), XErrorResp.class);
                serverException = new ServerException(false, errorResp.getErrCode(), errorResp.getErrMessage());
            } catch (IOException e) {
                serverException = new ServerException(true, response.getStatus(), response.getEntity(String.class));
            }
            if (exceptionConsumer != null) {
                exceptionConsumer.accept(serverException);
                throw new RuntimeException("The exception handler must throw exception");
            } else
                throw serverException;
        }
    }

    private ResponseDto _post(String path, RequestDto requestDto) {
        WebResource resource = Client.create().resource(PropertyHelper.getProperty("server_url") + path);
        ClientResponse response;
        try {
            response = resource.type(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).cookie(new NewCookie("JSESSIONID", authService.getSid())).post(ClientResponse.class, objectMapper.writeValueAsString(requestDto));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        authCookies(response.getCookies());
        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            if (response.hasEntity()) {
                try {
                    return objectMapper.readValue(response.getEntity(String.class), getResponseClass());
                } catch (IOException e) {
                    throw new RuntimeException("Server connection error");
                }
            } else
                return null;
        } else {
            ServerException serverException;
            try {
                XErrorResp errorResp = objectMapper.readValue(response.getEntity(String.class), XErrorResp.class);
                serverException = new ServerException(false, errorResp.getErrCode(), errorResp.getErrMessage());
            } catch (IOException e) {
                serverException = new ServerException(true, response.getStatus(), response.getEntity(String.class));
            }
            if (exceptionConsumer != null) {
                exceptionConsumer.accept(serverException);
                throw new RuntimeException("The exception handler must throw exception");
            } else
                throw serverException;
        }
    }

    private void authCookies(List<NewCookie> cookies) {
        cookies.stream().filter(cookie -> cookie.getName().equals("JSESSIONID")).forEach(cookie -> authService.setSid(cookie.getValue()));
    }

    @SuppressWarnings("unchecked")
    protected Class<RequestDto> getRequestClass() {
        return (Class<RequestDto>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    protected Class<ResponseDto> getResponseClass() {
        return (Class<ResponseDto>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
}
