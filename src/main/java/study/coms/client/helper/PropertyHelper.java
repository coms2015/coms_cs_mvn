package study.coms.client.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class PropertyHelper {
    private PropertyHelper() {}

    private static Map<String, String> dictionary = new HashMap<>();

    public static String getProperty(String name) {
        if (dictionary.containsKey(name))
            return dictionary.get(name);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("config.properties"));
            String value = properties.getProperty(name);
            dictionary.put(name, value);
            return value;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
