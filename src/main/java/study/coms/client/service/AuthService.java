package study.coms.client.service;

import study.coms.client.exception.SilentServerException;
import study.coms.client.helper.RestProvider;
import study.coms.exchange.message.XLoginReq;
import study.coms.exchange.message.XLogoutReq;
import study.coms.exchange.user.XUserDto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class AuthService {
    private boolean loggedIn = false;
    private Set<String> modules = new HashSet<>();
    private String sid = null;

    AuthService() {}

    public boolean login(String login, String password) {
        RestProvider<XLoginReq, XUserDto> provider = new RestProvider<XLoginReq, XUserDto>(error -> {
            if (error.getCode() == 1)
                throw new SilentServerException(error.getCode(), error.getMessage());
            else
                throw new RuntimeException(error.getMessage());
        }) {};
        loggedIn = false;
        XLoginReq request = new XLoginReq(login, password);
        XUserDto userDto = provider.post("/auth/login", request);
        loggedIn = true;
        modules.addAll(Arrays.asList(userDto.getRole().getModules().split(",")));
        return true;
    }

    public void logout() {
        loggedIn = false;
        modules.clear();
        new RestProvider<XLogoutReq, Object>() {}.post("/auth/logout", new XLogoutReq());
    }

    public boolean checkModule(String module) {
        return modules.contains(module);
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
