package study.coms.client.service;

import study.coms.client.helper.RestProvider;
import study.coms.exchange.goods.XGoodsDto;

import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class GoodsService {
    private final RestProvider<XGoodsDto, XGoodsDto> provider = new RestProvider<XGoodsDto, XGoodsDto>() {};

    public void getGoodsListAsync(Consumer<List<XGoodsDto>> onResult) {
        provider.getListAsync("/user/goods", onResult);
    }

    public void addGoodsAsync(XGoodsDto goodsDto, Consumer onResult) {
        provider.postAsync("/user/goods/add", goodsDto, onResult);
    }

    public void editGoodsAsync(XGoodsDto goodsDto, Consumer onResult) {
        provider.postAsync("/user/goods/edit", goodsDto, onResult);
    }

    public void removeGoodsAsync(XGoodsDto goodsDto, Consumer onResult) {
        provider.postAsync("/user/goods/remove", goodsDto, onResult);
    }
}
