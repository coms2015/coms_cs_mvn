package study.coms.client.service;

import study.coms.client.helper.RestProvider;
import study.coms.exchange.store.XStoreDto;

import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class StoreService {
    private final RestProvider<XStoreDto, XStoreDto> provider = new RestProvider<XStoreDto, XStoreDto>() {};

    public void getStoresListAsync(Consumer<List<XStoreDto>> onResult) {
        provider.getListAsync("/user/stores", onResult);
    }

    public void addStoreAsync(XStoreDto storeDto, Consumer onResult) {
        provider.postAsync("/user/stores/add", storeDto, onResult);
    }

    public void editStoreAsync(XStoreDto storeDto, Consumer onResult) {
        provider.postAsync("/user/stores/edit", storeDto, onResult);
    }

    public void removeStoreAsync(XStoreDto storeDto, Consumer onResult) {
        provider.postAsync("/user/stores/remove", storeDto, onResult);
    }
}
