package study.coms.client.service;

public final class ServicesProvider {
    private static volatile ServicesProvider instance;

    private ServicesProvider() {}

    public static ServicesProvider getInstance() {
        ServicesProvider local = instance;
        if (local == null) {
            synchronized (ServicesProvider.class) {
                local = instance;
                if (local == null) {
                    local = instance = new ServicesProvider();
                }
            }
        }
        return local;
    }

    private volatile AuthService authService = null;
    private volatile StoreService storeService = null;
    private volatile GoodsService goodsService = null;

    public AuthService getAuthService() {
        return authService == null ? authService = new AuthService() : authService;
    }

    public StoreService getStoreService() {
        return storeService == null ? storeService = new StoreService() : storeService;
    }

    public GoodsService getGoodsService() {
        return goodsService == null ? goodsService = new GoodsService() : goodsService;
    }
}
